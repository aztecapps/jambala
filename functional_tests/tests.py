from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest

class NewVisitorTest(unittest.TestCase):

  def setUp(self):
    self.browser = webdriver.Firefox()
    self.browser.implicitly_wait(3)

  def tearDown(self):
    self.browser.quit()

  def test_can_search_by_date(self):
    self.browser.get('http://localhost:8000')

    self.assertIn('Jambala', self.browser.title)
    header_text = self.browser.find_element_by_tag_name('h1').text
    self.assertIn('Jambala', header_text)

    #table = self.browser.find_element_by_id('results_table')
    #rows = table.find_elements_by_tag_name('t')
    #start_date = self.browser.find_element_by_id('start_date')
    #start_date.send_keys('')

if __name__ == '__main__':
  unittest.main(warnings='ignore')
