from django.conf.urls import url
from django.contrib import admin
from market import views as market_views

urlpatterns = [
  url(r'^$', market_views.home_page, name='home')
]
