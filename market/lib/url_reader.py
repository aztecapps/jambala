import pandas as pd
import time

class URLReader:

  _url = 'http://chartapi.finance.yahoo.com/instrument/1.0/GOOG/chartdata;type=quote;range=1d/csv'
  _offset = 32
  _names = ['Timestamp', 'Close', 'High', 'Low', 'Open', 'Volume']

  def read(self):
    self.data_frame = pd.read_csv(self._url, skiprows=self._offset, header=0, names=self._names)
    self.data_frame['Timestamp'] = self.data_frame['Timestamp'].map(self.to_date)
    return self.data_frame

  def to_date(self, epoch):
   return time.strftime('%d/%m/%Y %l:%M:%S %p', time.localtime(epoch))
