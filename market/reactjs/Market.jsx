import React from "react"
import { render } from "react-dom"

import {
  createStore,
  compose,
  applyMiddleware,
  combineReducers,
} from "redux"

import { Provider } from "react-redux"
import thunk from "redux-thunk"
import * as reducers from "./reducers"
import MarketContainer from "./containers/MarketContainer"

let finalCreateStore = compose(
  applyMiddleware(thunk),
  window.devToolsExtension ? window.devToolsExtension() : f => f
)(createStore)

let reducer = combineReducers(reducers)
let store = finalCreateStore(reducer)

class Market extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <MarketContainer />
      </Provider>
    )
  }
}

render(<Market/>, document.getElementById('Market'))
