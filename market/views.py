from django.shortcuts import render
from .lib.url_reader import URLReader

def home_page(request):
  data_table = URLReader()
  return render(request, 'market/home.html', { 'data_table': data_table.read().to_html(classes=['table', 'table-sm', 'table-striped', 'table-bordered', 'table-hoverable']) })
