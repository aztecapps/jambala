var path = require("path")
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')

var config = require('./webpack.base.config.js')

config.ip = 'localhost'
config.port = ':3333'

config.devtool = "#eval-source-map"

config.entry = {
  Market: [
    'webpack-dev-server/client?http://' + config.ip + config.port,
    'webpack/hot/only-dev-server',
    './market/reactjs/Market',
  ]
}

config.output.publicPath = 'http://' + config.ip + config.port + '/assets/bundles/'

config.plugins = config.plugins.concat([
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NoErrorsPlugin(),
  new BundleTracker({filename: './webpack-stats-local.json'}),
  new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('development'),
      'BASE_API_URL': JSON.stringify('http://' + config.ip + config.port + '/api/v1/'),
  }}),
])

config.module.loaders.push(
  { test: /\.jsx?$/, exclude: /node_modules/, loaders: ['react-hot', 'babel'] }
)

module.exports = config
